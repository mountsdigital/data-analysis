package com.mounts.dataanalysis.backend;

import com.mounts.dataanalysis.backend.annotations.Config;
import com.mounts.dataanalysis.backend.data.*;
import com.mounts.dataanalysis.backend.datapackage.DataPackage;
import com.mounts.dataanalysis.backend.datapackage.DataPackageConfig;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.lang.reflect.Constructor;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class DataReader {
    private String path;
    private DataPackageConfig config;

    public DataReader(String _path, DataPackageConfig config) {
        path = _path;
        this.config = config;
    }

    @SuppressWarnings("unchecked")
    private ArrayList[] readFile(Class klass) throws Exception {
        Constructor[] constructors = klass.getConstructors();
        Constructor constructor = null;

        for(Constructor c: constructors) {
            if (c.getParameterCount() > 0) {
                constructor = c; break;
            }
        }

        Config annotation = (Config) klass.getAnnotation(Config.class);
        int step = annotation.length();
        String name = annotation.name();

        String readPath = Utility.findFile(path, name);

        if (readPath.isEmpty()) throw new Exception("File: " + name + " not found.");

        byte[] bytes = Files.readAllBytes(Paths.get(readPath));

        ArrayList data = new ArrayList<>();
        ArrayList halt = new ArrayList<>();

        int errors = 0;
        double ltdkm = 0.00;//changing by chann

        HaltTime.Control ctrl = new HaltTime.Control();
        HaltTime.cumTime = 0;

        for(int i = 0; i < bytes.length; i += step) {
            try {
                Object instance = constructor.newInstance(Utility.subArray(bytes, i, step), "20");
                if (name.equals("SHORTMEM.DAT") || name.equals("LONGMEN.DAT")) {
                    ShortTerm shortLongMem = (ShortTerm) instance;

                    if (i == 0)
                        shortLongMem.resetEventMems();

                    shortLongMem.calculateTotalDistKm(ltdkm);

                    if (shortLongMem.getEventTwo().contains("Train Start")) {
                        ctrl.started = true;
                        ctrl.start = new DateTime(shortLongMem.getDateTimeMills(), DateTimeZone.UTC);

                        ctrl.runDist = 0;
                    }

                    if (ctrl.started) {
                        ctrl.runDist += shortLongMem.getDistance();
                    }

                    if (shortLongMem.getEventTwo().contains("Train Stop") && ctrl.started) {
                        ctrl.started = false;
                        ctrl.stop = new DateTime(shortLongMem.getDateTimeMills(), DateTimeZone.UTC);
                        ctrl.cumDist += ctrl.runDist;

                        HaltTime cht = new HaltTime(ctrl);
                        halt.add(cht);

                        if (halt.size() > 1) ((HaltTime) halt.get(halt.size() - 2)).setHaltTime(cht.getDateTimeMills());
                    }

                    ltdkm = shortLongMem.getTotalDisKm();
                }


                data.add(instance);

            } catch (Exception e){ errors ++; }
        }

        System.out.println(klass.getName().replace("com.mounts.dataanalysis.backend.data.", "") + " ERRORS : " + errors);
        return new ArrayList[]{data, halt};
    }

    @SuppressWarnings("unchecked")
    public DataPackage read() throws Exception {
        HashMap<String, ArrayList> readData = new HashMap<>();
        HashMap<String, Class> dataClasses = config.getDataClasses();

        ArrayList<HaltTime> haltTimeDataList = new ArrayList<>();

        for(String key: dataClasses.keySet()) {
            Class value = dataClasses.get(key);
            ArrayList[] fileData = readFile(value);
            readData.put(key, fileData[0]);
            haltTimeDataList.addAll(fileData[1]);
        }

        return new DataPackage(readData, haltTimeDataList);
    }
}
