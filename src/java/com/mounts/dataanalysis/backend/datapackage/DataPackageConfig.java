package com.mounts.dataanalysis.backend.datapackage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class DataPackageConfig implements Serializable {
    private boolean shouldGenerateDTWandTDW;
    private HashMap<String, Class> dataClasses;

    public DataPackageConfig(boolean sg) {
        shouldGenerateDTWandTDW = sg;
        dataClasses = new HashMap<>();
    }

    public void addDataClass(Class c) {
        dataClasses.put(c.getSimpleName(), c);
    }

    public HashMap<String, Class> getDataClasses() {
        return dataClasses;
    }

    public boolean shouldGenerateDTW() {
        return shouldGenerateDTWandTDW;
    }

    public void generateDTW() {
        shouldGenerateDTWandTDW = true;
    }
}
