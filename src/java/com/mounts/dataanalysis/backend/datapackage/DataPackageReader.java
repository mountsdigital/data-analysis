package com.mounts.dataanalysis.backend.datapackage;

import com.mounts.dataanalysis.Log;
import com.mounts.dataanalysis.backend.annotations.Config;
import com.mounts.dataanalysis.backend.data.DriverTrainWiseData;
import com.mounts.dataanalysis.backend.data.HaltTime;
import com.mounts.dataanalysis.backend.data.JSONSerializable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

public class DataPackageReader {
    public static String TAG = DataPackageReader.class.getSimpleName();

    private String dataDirectory;
    private JSONParser parser;
    private boolean shouldLoadDTW;

    public DataPackageReader(String pkgName) {
        dataDirectory = DataPackage.DATA_PATH + "/" + pkgName;
        parser = new JSONParser();
    }

    private HashMap<String, Class> readClassList() throws ClassNotFoundException, IOException {
        ObjectInputStream configInputStream = new ObjectInputStream(new FileInputStream( dataDirectory + "/CONF.DAT"));
        DataPackageConfig packageConfig = (DataPackageConfig) configInputStream.readObject();

        this.shouldLoadDTW = packageConfig.shouldGenerateDTW();

        return packageConfig.getDataClasses();
    }

    private ArrayList<HaltTime> readHaltTimeList() throws ParseException, IOException {
        String content = new String(Files.readAllBytes(new File(dataDirectory + "/HTD.DAT").toPath()));
        JSONArray jsonDataArray = (JSONArray) parser.parse(content);
        ArrayList<HaltTime> dataArray = new ArrayList<>();

        for(Object raw: jsonDataArray) {
            HaltTime haltTime = new HaltTime();
            haltTime.fromJSONObject((JSONObject) raw);
            dataArray.add(haltTime);
        }

        return dataArray;
    }

    private ArrayList<DriverTrainWiseData> readDTRList() throws IOException, ParseException {
        if (!shouldLoadDTW) return new ArrayList<>();

        String content = new String(Files.readAllBytes(new File(dataDirectory + "/DTW.DAT").toPath()));
        JSONArray jsonDataArray = (JSONArray) parser.parse(content);
        ArrayList<DriverTrainWiseData> dataArray = new ArrayList<>();

        for(Object raw: jsonDataArray) {
            DriverTrainWiseData dtr = new DriverTrainWiseData();
            dtr.fromJSONObject((JSONObject) raw);
            dataArray.add(dtr);
        }

        return dataArray;
    }

    @SuppressWarnings("unchecked")
    private HashMap<String, ArrayList> readMainPackageFiles() throws ParseException, ClassNotFoundException, IOException, IllegalAccessException, InstantiationException {
        HashMap<String, ArrayList> mainPackageDataMap = new HashMap<>();
        HashMap<String, Class> dataClassList = readClassList();

        for(String key: dataClassList.keySet()) {
            Class aClass = dataClassList.get(key);
            String fileName = ((Config) aClass.getAnnotation(Config.class)).name();
            String content = new String(Files.readAllBytes(new File(dataDirectory + "/" + fileName).toPath()));

            JSONArray jsonDataArray = (JSONArray) parser.parse(content);
            ArrayList parsedDataArray = new ArrayList();

            for(Object raw: jsonDataArray) {
                JSONSerializable instance = (JSONSerializable) aClass.newInstance();
                instance.fromJSONObject((JSONObject) raw);
                parsedDataArray.add(instance);
            }

            mainPackageDataMap.put(key, parsedDataArray);
        }

        return mainPackageDataMap;
    }

    @SuppressWarnings("unchecked")
    public DataPackage read() {
        DataPackage pkg;

        try {
            pkg =  new DataPackage(readMainPackageFiles(), readHaltTimeList(), readDTRList());

        } catch (Exception e) {
            Log.debug(TAG, "read", e.getClass().getSimpleName());
            Log.debug(TAG, "read", e.getMessage());

            pkg = new DataPackage();
        }

        return pkg;
    }
}
