package com.mounts.dataanalysis.backend.datapackage;

import com.mounts.dataanalysis.backend.annotations.Config;
import com.mounts.dataanalysis.backend.data.DriverTrainWiseData;
import com.mounts.dataanalysis.backend.data.HaltTime;
import com.mounts.dataanalysis.backend.data.JSONSerializable;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;

public class DataPackageWriter {
    private DataPackage pkg;
    private DataPackageConfig config;
    private String dataDirectoryPath;

    public DataPackageWriter(DataPackage pkg, DataPackageConfig config) {
        this.pkg = pkg;
        this.config = config;
        this.dataDirectoryPath = DataPackage.DATA_PATH;
    }

    @SuppressWarnings("unchecked")
    public void write(String name, String type) throws IOException, ParseException {
        String outputPath = String.format("%s/%s-%s-%s",
                this.dataDirectoryPath, name, type,
                DateTime.now().toString(
                        DateTimeFormat.forPattern("MM-dd-YY-hh-mm-ss-a")//MM-dd-YY-hh-mm-ss-a
                )
        );

        File outputDir = new File(outputPath);
        if (!outputDir.exists()) outputDir.mkdir();
        Charset utf8 = Charset.forName("UTF-8");

        HashMap<String, Class> dataClassNameMap = config.getDataClasses();
        for(String key: dataClassNameMap.keySet()) {
            Class aClass = dataClassNameMap.get(key);
            String dataKey = aClass.getSimpleName();
            String dataName = ((Config) aClass.getAnnotation(Config.class)).name();

            JSONArray jsonArrayOut = new JSONArray();

            ArrayList dataList = pkg.getData().get(dataKey);
            for(Object raw: dataList) {
                JSONSerializable base = (JSONSerializable) raw;
                jsonArrayOut.add(base.toJSONObject());
            }

            FileOutputStream fileOut = new FileOutputStream(outputPath + "/" + dataName);
            fileOut.write(jsonArrayOut.toJSONString().getBytes(utf8));
            fileOut.close();
        }

        //TODO write original hex data classes to JSON

        JSONArray haltTimeDataOutJson = new JSONArray(),
                driverTrainWiseDataOutJosn = new JSONArray();

        for(DriverTrainWiseData d: pkg.getDriverTrainWiseDataList()) driverTrainWiseDataOutJosn.add(d.toJSONObject());
        for(HaltTime h: pkg.getHaltTimeDataList()) haltTimeDataOutJson.add(h.toJSONObject());

        FileOutputStream dtwOut = new FileOutputStream(outputPath + "/DTW.DAT"),
                haltOut = new FileOutputStream(outputPath + "/HTD.DAT"),
                confOut = new FileOutputStream(outputPath + "/CONF.DAT");

        ObjectOutputStream confObjOut = new ObjectOutputStream(confOut);

        haltOut.write(haltTimeDataOutJson.toJSONString().getBytes(utf8));
        dtwOut.write(driverTrainWiseDataOutJosn.toJSONString().getBytes(utf8));
        confObjOut.writeObject(config);

        haltOut.close();
        dtwOut.close();
        confObjOut.close();
        confOut.close();
    }
}
