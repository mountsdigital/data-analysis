package com.mounts.dataanalysis.backend.datapackage;

import com.mounts.dataanalysis.backend.Utility;
import com.mounts.dataanalysis.backend.data.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class DataPackage {

  public static String DATA_PATH = System.getProperty("user.dir") + "/data";

//    public static String DATA_PATH = "C:\\mkt\\data";
    private HashMap<String, ArrayList> data;
    private ArrayList<DriverTrainWiseData> driverTrainWiseDataList;
    private ArrayList<HaltTime> haltTimeDataList;

//    private static ArrayList<String> timeList = new ArrayList<>();
//    private static ArrayList<Number> speedList = new ArrayList<>();
//    private static ArrayList<Number> disList = new ArrayList<>();
//    private static ArrayList<String> dateList=new ArrayList<>();

    public DataPackage() {
        driverTrainWiseDataList = new ArrayList<>();
        haltTimeDataList = new ArrayList<>();
        data = new HashMap<>();

    }

    @SuppressWarnings("unchecked")
    public DataPackage(HashMap<String, ArrayList> data, ArrayList<HaltTime> haltTimeDataList) {
        driverTrainWiseDataList = new ArrayList<>();
        this.haltTimeDataList = haltTimeDataList;
        this.data = data;
    }

    public DataPackage(HashMap<String, ArrayList> data, ArrayList<HaltTime> haltTimeDataList, ArrayList<DriverTrainWiseData> driverTrainWiseDataList) {
        this(data, haltTimeDataList);

        this.driverTrainWiseDataList = driverTrainWiseDataList;
    }

    public void generateDTR() {
        boolean shouldContinueST = data.keySet().containsAll(Arrays.asList(ShortTerm.class.getSimpleName(), OperatorData.class.getSimpleName()));
        boolean shouldContinueLT = data.keySet().containsAll(Arrays.asList(LongTerm.class.getSimpleName(), OperatorData.class.getSimpleName()));
        ArrayList<OperatorData> operatorDataList = getOperatorDataList();


        if (!(shouldContinueST || shouldContinueLT)) return;

        DateTimeFormatter dtf = DateTimeFormat.forPattern("MM-dd-YY HH:mm:ss");

        for (int i = 0; i < operatorDataList.size(); i++) {
            OperatorData operatorData = operatorDataList.get(i), nextOperatorData;
            long start = operatorData.getDateTimeMills(), end;

            String nextDate , nextTime;

            if (i + 1 < operatorDataList.size()) {
                nextOperatorData = operatorDataList.get(i + 1);
                end = nextOperatorData.getDateTimeMills();

                nextDate = nextOperatorData.getDate();
                nextTime = nextOperatorData.getTime();


            }
            else {
                nextDate = "01-01-50";
                nextTime = "21:59:59";
                end = DateTime.parse(nextDate + " " + nextTime, dtf).getMillis();
            }


            for (HaltTime ht : haltTimeDataList) {
                if (start <= ht.getDateTimeMills() && ht.getDateTimeMills() <= end) {
                    ht.setDriverTrainInfo(operatorData.getDriverID(), operatorData.getTrainNumber());
                }
            }

            DateTime startDT = DateTime.parse(operatorData.getDate() + " " + operatorData.getTime(), dtf);
            DateTime endDT = DateTime.parse(nextDate + " " + nextTime, dtf);

            DriverTrainWiseData driverTrainWiseData = new DriverTrainWiseData(startDT, endDT);
            driverTrainWiseData.setOperator(operatorData);

            ArrayList shortTerms = getTermDataBetweenDate(shouldContinueST, driverTrainWiseData);

            int totalDistance = 0, numberOfOverSpeed = 0, maxSpeed = 0;

            for (Object shortTerm : shortTerms) {
                ShortTerm term = (ShortTerm) shortTerm;

                int speed = term.getSpeed();
                double distance = term.getDistance();

                totalDistance += distance;

                if (speed > maxSpeed) {
                    maxSpeed = speed;
                }

                if (speed > operatorData.getSpeedLimitKMPH()) {
                    numberOfOverSpeed++;
                }

            }

            String totalTime = Utility.toHMS(end - start);
            driverTrainWiseData.setTotalDistance(totalDistance);
            driverTrainWiseData.setTotalTime(totalTime);
            driverTrainWiseData.setNumberOfOverSpeed(numberOfOverSpeed);
            driverTrainWiseData.setMaxSpeed(maxSpeed);

            driverTrainWiseDataList.add(driverTrainWiseData);
        }
    }

    @SuppressWarnings("unchecked")
    private <T> ArrayList<T> getTermDataBetweenDate(boolean sg, DriverTrainWiseData dtr) {

        ArrayList<T> results = new ArrayList<>();
        ArrayList shortLongTermDataList = sg ? getShortLongTermDataList() : getLongTermDataList();

        long start = dtr.getDateTimeMills();
        long end = dtr.getDateTimeEndMills();
//        timeList.clear();
//        speedList.clear();
//        disList.clear();


        for (Object data : shortLongTermDataList) {
            ShortTerm term = (ShortTerm) data;
            long dtm = term.getDateTimeMills();
            System.out.println("time:"+term.getTime());
//            dateList.add(term.getDate());
//            timeList.add(term.getTime());
//            speedList.add(term.getSpeed());
//            disList.add(term.getDistance());
            boolean condition = end < 0 ? start <= dtm : start <= dtm && dtm < end;
            if (condition) {
                term.setDriverTrainWiseData(dtr);
                results.add((T) term);
            }
        }

        return results;
    }

    @SuppressWarnings("unchecked")
    public ArrayList<ERRLog> getErrLogDataList() {
        return data.get(ERRLog.class.getSimpleName());
    }

    @SuppressWarnings("unchecked")
    public ArrayList<MainConfig> getMainConfigDataList() {
        return data.get(MainConfig.class.getSimpleName());
    }

    @SuppressWarnings("unchecked")
    public ArrayList<OperatorData> getOperatorDataList() {
        return data.get(OperatorData.class.getSimpleName());
    }

    @SuppressWarnings("unchecked")
    public ArrayList<ShortTerm> getShortLongTermDataList() {
        return data.get(ShortTerm.class.getSimpleName());
    }

    @SuppressWarnings("unchecked")
    public ArrayList<LongTerm> getLongTermDataList() {
        return data.get(LongTerm.class.getSimpleName());
    }

    @SuppressWarnings("unchecked")
    public ArrayList<ShortTerm> getShortTermDataList() {
        return data.get(ShortTerm.class.getSimpleName());
    }

    public ArrayList<DriverTrainWiseData> getDriverTrainWiseDataList() {
        return driverTrainWiseDataList;
    }

    public ArrayList<HaltTime> getHaltTimeDataList() {
        return haltTimeDataList;
    }

    public HashMap<String, ArrayList> getData() {
        return data;
    }

//    public ArrayList<String> getTimeList() {
//        return timeList;
//    }
//
//    public ArrayList<Number> getSpeedList() {
//        return speedList;
//    }
//
//    public ArrayList<Number> getDisList() {
//        return disList;
//    }
//
//    public ArrayList<String> getDateList(){
//        return dateList;
//    }
}
