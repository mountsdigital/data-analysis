package com.mounts.dataanalysis.backend.data;

import com.mounts.dataanalysis.backend.Utility;
import com.mounts.dataanalysis.backend.annotations.Config;
import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

@Config(length =  7, name = "ERRLOG.DAT")
public class ERRLog extends BaseData implements JSONSerializable {
    private int errorEvent;

    public String getErrorEvent() {
        return errorEvents[errorEvent];
    }

    public ERRLog() {}

    public ERRLog(byte[] data, String ypre) {
        super(data, 0, ypre);
        errorEvent = Utility.byte2dec(data[6]);
    }

    @Override
    @SuppressWarnings("unchecked")
    public JSONObject toJSONObject() {
        JSONObject json = new JSONObject();
        JSONObject dateTime = new JSONObject();

        dateTime.put("start", getDateTimeMills());
        json.put("dateTime", dateTime);
        json.put("errorEvent", errorEvent);

        return json;
    }

    @Override
    public void fromJSONObject(JSONObject json) {
        dateTime = new DateTime(((JSONObject) json.get("dateTime")).get("start"));
        errorEvent = Utility.longToInt(json.get("errorEvent"));
    }

    //region Fix Error Event Data
    private final String[] errorEvents = {
        "",
        "M-det Feed back Error @running",
        "M-det Feed back Error @0 speed",
        "Speed Det1 shows >20.000 RPM",
        "Speed Det2 shows >20.000 RPM",
        "M-det/S-Motor faulty",
        "G-det/S-Motor faulty",
        "LCD faulty",
        "RTC faulty",
        "Initialisation Error in MC",
        "2 Sectors Falty in MC",
        "2 Sectors faulty in Recorder",
        "Detector M faulty",
        "Detector G faulty",
        "Speed Sensor Detector1 faulty",
        "Speed Sensor Detector2 faulty",
        "200 Time outs Errors",
        "200 Check sum Errors",
        "No Data received for 10 secs",
        "Speed Detector1 recovered",
        "Speed Detector2 recovered",
        "Default OPR record loaded",
        "Default CFG record loaded",
        "RTC Odometer faulty & Reset",
        "User changed Odometer setting",
        "IND M-det Feed back Error @run",
        "IND M-det FB Error @0 speed",
        "IND M-det/S-Motor faulty",
        "IND G-det/S-Motor faulty",
        "INDICATOR LCD Faulty",
        "Energy measuring channel noisy",
        "Energy counts Reset-RTC Error",
        "All counts Reset thru Key Bord",
        "Memory Card Loaded now",
        "Memory Card Removed",
        "",
        "",
        "",
        "",
        "PC Downloading starts",
        "PC Downloading Ends",
        "PC Downloading Failed",
        "Pendrive Downloading starts",
        "Pendrive Downloading Ends",
        "Pendrive Downloading Failed",
        "RTC restarted - ",
        "Configuration checksum fail",
        "No Communciation with indictor",
        "Indicator Acknowledge err",
        "Indicator tx postponed",
        "Indicator forced tx mode",
        "Indicator comm Recovered",
        "Indicator Acknowledge Ok",
        "Indicator transmission timely",
        "Indicator forcd tx mode-recvrd"
    };
    //endregion
}
