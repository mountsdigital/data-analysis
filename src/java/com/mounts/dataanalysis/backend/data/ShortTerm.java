package com.mounts.dataanalysis.backend.data;

import com.mounts.dataanalysis.backend.Utility;
import com.mounts.dataanalysis.backend.annotations.Config;
import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

@Config(length =  11, name = "SHORTMEM.DAT")
public class ShortTerm extends BaseData implements JSONSerializable {
    public static int[][] eventmem = new int[][] {
            {-1, -1, -1, -1, -1, -1, -1, -1},
            {-1, -1, -1, -1, -1, -1, -1, -1},
    };

    private DriverTrainWiseData driverTrainWiseData;
    private int speed, breakPressure,distance;
    private double totalDisKm;
    private String eventOne, eventTwo;
    private boolean isOverSpeed = false;

    private final String[][] events = {
        {"Memory Card Load", "Traction ON", "Break Status: Hold", "DB Status: ON", "VCD Switch: PUSH", "Horn Switch: PUSH", "Alarm ON", "Emergency ON"},
        {"Power ON", "Train Start", "Time Change", "MEM Download Start", "Loco Type Change", "Wheel Dia Change", "Over Speed", "Door Open"}
    };

    private final String[][] eventsOff = {
            {"Memory Card Remove", "Traction OFF", "Break Status: Release", "DB Status: OFF", "VCD Switch: Release", "Horn Switch: Release", "Alarm OFF", "Emergency OFF"},
            {"Power OFF", "Train Stop", "Time Un-Change", "MEM Download Stop", "Loco Type Un-Change", "Wheel Dia Un-Change", "Normal Speed", "Door Close"}
    };

    public void resetEventMems() {
        eventmem = new int[][] {
                {-1, -1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, -1, -1},
        };
    }

    public ShortTerm() {}

    public ShortTerm(byte[] data, String ypre) {
        super(data, 0, ypre);



        speed = Utility.byte2dec(data[6]);
        distance = Utility.byte2dec(data[7]);

        breakPressure = Utility.byte2dec(data[8]);

        eventOne = getActiveEvents(1, data[9]);
        eventTwo = getActiveEvents(2, data[10]);
    }

    public double getTotalDisKm() {//changing int to double by chann
        return totalDisKm;
    }

    public void calculateTotalDistKm(double ltdkm) {
        totalDisKm = distance / 1000.00+ ltdkm;
    }

    private String getActiveEvents(int number, byte data) {
        String bin = Utility.byte2bin(data);
        bin = new StringBuilder(bin).reverse().toString();

        if (number == 2 && bin.charAt(6) == '1') isOverSpeed = true;

        number --;

        StringBuilder events = new StringBuilder();

        int i = 0;
        for(char c: bin.toCharArray()) {
            if (eventmem[number][i] == -1) {
                if (c == '1') {
                    eventmem[number][i] = 1;
                    events.append(this.events[number][i]);
                    events.append(" ");
                }
            } else {
                String activateStatus = eventmem[number][i] + "";

                if (!activateStatus.equals(c + "")) {
                    if (c == '1') {
                        eventmem[number][i] = 1;
                        events.append(this.events[number][i]);
                        events.append(" ");
                    } else if (c == '0') {
                        eventmem[number][i] = 0;
                        events.append(this.eventsOff[number][i]);
                        events.append(" ");
                    }
                }
            }

            i++;
        }

        return events.toString().trim();
    }

    public void setDriverTrainWiseData(DriverTrainWiseData driverTrainWiseData) {
        this.driverTrainWiseData = driverTrainWiseData;
    }

    public DriverTrainWiseData getDriverTrainWiseData() {
        return driverTrainWiseData;
    }

    public int getSpeed() {
        return speed;
    }

    public int getBreakPressure() {
        return breakPressure;
    }

    public String getEventOne() {
        return eventOne;
    }

    public String getEventTwo() {
        return eventTwo;
    }

    public int getDistance() {
        return distance;
    }

    public boolean isOverSpeed() {
        return isOverSpeed;
    }


    @Override
    @SuppressWarnings("unchecked")
    public JSONObject toJSONObject() {
        JSONObject dateTime = new JSONObject();
        dateTime.put("start", getDateTimeMills());
        JSONObject json = new JSONObject();

        json.put("dateTime", dateTime);
        json.put("speed", speed);
        json.put("breakPressure", breakPressure);
        json.put("distance", distance);
        json.put("totalDisKm", totalDisKm);

        json.put("eventOne", eventOne);
        json.put("eventTwo", eventTwo);
        json.put("isOverSpeed", isOverSpeed);

        if (driverTrainWiseData != null)
            json.put("driverTrainWiseData", driverTrainWiseData.toJSONObject());

        return json;
    }

    @Override
    public void fromJSONObject(JSONObject json) throws ParseException {
        this.speed = Utility.longToInt(json.get("speed"));
        this.breakPressure = Utility.longToInt(json.get("breakPressure"));
        this.distance = Utility.longToInt(json.get("distance"));
        this.totalDisKm =Utility.longToDouble(json.get("totalDisKm"));//changing by chann
        this.distance = Utility.longToInt(json.get("distance"));
        this.eventOne = (String) json.get("eventOne");
        this.eventTwo = (String) json.get("eventTwo");
        this.isOverSpeed = (boolean) json.get("isOverSpeed");

        Object jsonDTR = json.get("driverTrainWiseData");

        if (jsonDTR != null) {
            driverTrainWiseData = new DriverTrainWiseData();
            driverTrainWiseData.fromJSONObject((JSONObject) jsonDTR);
        }

        dateTime = new DateTime(((JSONObject)json.get("dateTime")).get("start"));

    }
}
