package com.mounts.dataanalysis.backend.data;

import com.mounts.dataanalysis.backend.Utility;
import org.joda.time.DateTime;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class HaltTime extends BaseDataWithEnd implements JSONSerializable {
    public static long cumTime = 0;
    public static class Control {
        public boolean started = false;
        public DateTime start, stop;
        public int runDist = 0, cumDist = 0;
    }

    private int runDistance, cumulativeDistance;
    private long runTime, cumulativeTime, haltTime;
    private String driverID, trainNo;

    public HaltTime(Control ctrl) {
        super(ctrl.start, ctrl.stop);

        runDistance = ctrl.runDist;
        cumulativeDistance = ctrl.cumDist;

        runTime = getDateTimeEndMills() - getDateTimeMills();
        HaltTime.cumTime += runTime;
        cumulativeTime = HaltTime.cumTime;
    }

    public HaltTime() {}

    public void setDriverTrainInfo(String did, String tno) {
        driverID = did;
        trainNo = tno;
    }

    public void setHaltTime(long nextStartDT) {
        haltTime = nextStartDT - getDateTimeEndMills();
    }

    public String getDriverID() {
        return driverID;
    }

    public String getTrainNo() {
        return trainNo;
    }

    public int getRunDistance() {
        return runDistance;
    }

    public int getCumulativeDistance() {
        return cumulativeDistance;
    }

    public String getRunTime() {
        return Utility.toHMS(runTime);
    }

    public String getCumulativeTime() {
        return Utility.toHMS(cumulativeTime);
    }

    public String getHaltTime() {
        return Utility.toHMS(haltTime);
    }

    @SuppressWarnings("unchecked")
    public JSONObject toJSONObject() {
        JSONObject json = new JSONObject();
        JSONObject dateTime  = new JSONObject();

        dateTime.put("start", getDateTimeMills());
        dateTime.put("end", getDateTimeEndMills());

        json.put("driverID", driverID);
        json.put("trainNo", trainNo);
        json.put("runDistance", runDistance);
        json.put("cumulativeDistance", cumulativeDistance);
        json.put("runTime", runTime);
        json.put("cumulativeTime", cumulativeTime);
        json.put("haltTime", haltTime);
        json.put("dateTime", dateTime);

        return json;
    }

    @Override
    public void fromJSONObject(JSONObject json) {
        JSONObject dateTime = (JSONObject) json.get("dateTime");

        DateTime start = new DateTime(dateTime.get("start"));
        DateTime end = new DateTime(dateTime.get("end"));

        super.dateTime = start;
        super.dateTimeEnd = end;

        driverID = (String) json.get("driverID");
        trainNo = (String) json.get("trainNo");
        runDistance = Utility.longToInt(json.get("runDistance"));
        cumulativeDistance = Utility.longToInt(json.get("cumulativeDistance"));
        runTime = (long) json.get("runTime");
        cumulativeTime = (long) json.get("cumulativeTime");
        haltTime = (long) json.get("haltTime");
    }
}
