package com.mounts.dataanalysis.backend.data;

import com.mounts.dataanalysis.backend.Utility;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

abstract public class BaseData {
    protected DateTime dateTime;

    public BaseData() {}

    public BaseData(byte[] data, int dateStart, String ypre) {
        dateTime = Utility.parseDate(Utility.subArray(data, dateStart, 6), ypre);//for current date and time
    }

    public BaseData(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    public long getDateTimeMills() {

        return dateTime.getMillis();
    }

    public String getDate() {
        return DateTimeFormat.forPattern("MM-dd-YY").print(dateTime);
    }//startDate

    public String getTime() {
        return DateTimeFormat.forPattern("HH:mm:ss").print(dateTime);
    }//startTime
}
