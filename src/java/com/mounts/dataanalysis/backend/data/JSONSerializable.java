package com.mounts.dataanalysis.backend.data;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;


public interface JSONSerializable {
    JSONObject toJSONObject();
    void fromJSONObject(JSONObject json) throws ParseException;
}
