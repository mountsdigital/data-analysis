package com.mounts.dataanalysis.backend.data;

import com.mounts.dataanalysis.backend.Utility;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

abstract public class BaseDataWithEnd extends BaseData {
    protected DateTime dateTimeEnd;

    public BaseDataWithEnd() {};

    public BaseDataWithEnd(byte[] data, int dateStart, int dateEnd, String ypre) {
        super(data, dateStart, ypre);
        dateTimeEnd = Utility.parseDate(Utility.subArray(data, dateEnd, 6), ypre);
    }

    public BaseDataWithEnd(DateTime start, DateTime end) {
        super(start);
        this.dateTimeEnd = end;
    }

    public long getDateTimeEndMills() {
        return dateTimeEnd.getMillis();
    }

    public String getDateEnd() {
        String end = DateTimeFormat.forPattern("MM-dd-YY").print(dateTimeEnd);
        return end.equals("01-01-50") ? "" : end;
    }

    public String getTimeEnd() {
        return getDateEnd().isEmpty() ? "" : DateTimeFormat.forPattern("HH:mm:ss").print(dateTimeEnd);
    }
}
