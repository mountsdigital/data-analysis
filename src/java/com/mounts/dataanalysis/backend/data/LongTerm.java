package com.mounts.dataanalysis.backend.data;

import com.mounts.dataanalysis.backend.Utility;
import com.mounts.dataanalysis.backend.annotations.Config;

@Config(length =  11, name = "LONGMEM.DAT")
public class LongTerm extends ShortTerm {
    public LongTerm() {}

    public LongTerm(byte[] data, String ypre) {
        super(data, ypre);
    }
}
