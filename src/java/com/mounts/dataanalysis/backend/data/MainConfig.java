package com.mounts.dataanalysis.backend.data;

import com.mounts.dataanalysis.backend.Utility;
import com.mounts.dataanalysis.backend.annotations.Config;
import org.joda.time.DateTime;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

@Config(length = 22, name = "MCFGWR.DAT")
public class MainConfig extends BaseData implements JSONSerializable {
    private int recorderSNO;
    private int dialKMPH;
    private int mode;
    private int longTermSecond, shortTermSecond;
    private int locoWtTonnes;
    private String speedDependentContact;
    private int[] speedDependentContactA;

    public MainConfig() {}

    public MainConfig(byte[] data, String ypre) {
        super(data, 0, ypre);

        recorderSNO = Utility.bytes2dec(Utility.subArray(data, 6, 2));
        dialKMPH = Utility.byte2dec(data[8]);
        mode = Utility.byte2dec(data[9]);
        longTermSecond = Utility.byte2dec(data[10]);
        shortTermSecond = Utility.byte2dec(data[11]);
        speedDependentContactA = Utility.bytes2ints(Utility.subArray(data, 12, 8));
        locoWtTonnes = Utility.bytes2dec(Utility.subArray(data, 20, 2));

        speedDependentContact = getSpeedDependentContactA();
    }

    @Override
    @SuppressWarnings("unchecked")
    public JSONObject toJSONObject() {
        JSONObject json = new JSONObject();
        JSONObject dateTime = new JSONObject();

        dateTime.put("start", getDateTimeMills());
        json.put("dateTime", dateTime);
        json.put("recorderSNO", recorderSNO);
        json.put("dialKMPH", dialKMPH);
        json.put("mode", mode);
        json.put("longTermSecond", longTermSecond);
        json.put("shortTermSecond", shortTermSecond);
        json.put("locoWtTonnes",locoWtTonnes);
        json.put("speedDependentContact", speedDependentContact);

        JSONArray sDCA = new JSONArray();

        for(int s: speedDependentContactA) {
            sDCA.add(s);
        }

        json.put("speedDependentContactA", sDCA);

        return json;
    }

    @Override
    public void fromJSONObject(JSONObject json) {
        this.recorderSNO = Utility.longToInt(json.get("recorderSNO"));
        this.dialKMPH = Utility.longToInt(json.get("dialKMPH"));
        this.mode = Utility.longToInt(json.get("mode"));
        this.longTermSecond = Utility.longToInt(json.get("longTermSecond"));
        this.shortTermSecond = Utility.longToInt(json.get("shortTermSecond"));
        this.locoWtTonnes = Utility.longToInt(json.get("locoWtTonnes"));
        this.speedDependentContact = (String) json.get("speedDependentContact");

        dateTime = new DateTime(((JSONObject)json.get("dateTime")).get("start"));

        JSONArray sDCA = (JSONArray) json.get("speedDependentContactA");
        speedDependentContactA = new int[sDCA.size()];

        int i = 0;
        for (Object s: sDCA){
            speedDependentContactA[i++] =  Utility.longToInt(s);
        }
    }

    public int getRecorderSNO() {
        return recorderSNO;
    }

    public int getDialKMPH() {
        return dialKMPH;
    }

    public int getMode() {
        return mode;
    }

    public int getLongTermSecond() {
        return longTermSecond;
    }

    public int getShortTermSecond() {
        return shortTermSecond;
    }

    public int getLocoWtTonnes() {
        return locoWtTonnes;
    }

    public String getSpeedDependentContact() {
        return speedDependentContact;
    }

    public String getSpeedDependentContactA() {
        // "1-off 1-on 2-off 2-on 3-off 3-on 4-off 4-on"
        return String.format(
                "  %d     %d    %d     %d    %d     %d    %d     %d",
                speedDependentContactA[0],
                speedDependentContactA[1],
                speedDependentContactA[2],
                speedDependentContactA[3],
                speedDependentContactA[4],
                speedDependentContactA[5],
                speedDependentContactA[6],
                speedDependentContactA[7]
        );
    }

    public int getOneOff() {
        return speedDependentContactA[0];
    }

    public int getOneOn() {
        return speedDependentContactA[1];
    }

    public int getTwoOff() {
        return speedDependentContactA[2];
    }

    public int getTwoOn() {
        return speedDependentContactA[3];
    }

    public int getThreeOff() {
        return speedDependentContactA[4];
    }

    public int getThreeOn() {
        return speedDependentContactA[5];
    }

    public int getFourOff() {
        return speedDependentContactA[6];
    }

    public int getFourOn() {
        return speedDependentContactA[7];
    }
}
