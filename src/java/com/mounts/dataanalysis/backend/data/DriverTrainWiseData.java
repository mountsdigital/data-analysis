package com.mounts.dataanalysis.backend.data;

import com.mounts.dataanalysis.backend.Utility;
import org.joda.time.DateTime;
import org.json.simple.JSONObject;

import java.io.Serializable;

public class DriverTrainWiseData extends BaseDataWithEnd implements Serializable, JSONSerializable {
    private String locoNumber;
    private String trainNumber;
    private String driverID;
    private String totalTime;

    private OperatorData operator;

    private int speedLimitKMPH;
    private int wheelDiameter;

    private int totalDistance;
    private int numberOfOverSpeed, maxSpeed;

    public DriverTrainWiseData(DateTime start, DateTime end) {
        super(start, end);
    }

    public DriverTrainWiseData() {
    }

    @SuppressWarnings("unchecked")
    public JSONObject toJSONObject() {
        JSONObject json = new JSONObject();
        JSONObject dateTime = new JSONObject();

        dateTime.put("start", getDateTimeMills());
        dateTime.put("end", getDateTimeEndMills());

        json.put("operator", operator.toJSONObject());
        json.put("dateTime", dateTime);
        json.put("locoNumber", locoNumber.trim());
        json.put("trainNumber", trainNumber.trim());
        json.put("driverID", driverID.trim());
        json.put("totalTime", totalTime.trim());
        json.put("speedLimitKMPH", speedLimitKMPH);
        json.put("wheelDiameter", wheelDiameter);
        json.put("totalDistance", totalDistance);
        json.put("numberOfOverSpeed", numberOfOverSpeed);
        json.put("maxSpeed", maxSpeed);

        return json;
    }

    @Override
    public void fromJSONObject(JSONObject driverTrainDataRaw) {
        JSONObject dateTime = (JSONObject) driverTrainDataRaw.get("dateTime");

        DateTime start = new DateTime(dateTime.get("start"));
        DateTime end = new DateTime(dateTime.get("end"));

        super.dateTime = start;
        super.dateTimeEnd = end;

        operator = new OperatorData();
        operator.fromJSONObject((JSONObject) driverTrainDataRaw.get("operator"));

        locoNumber = (String) driverTrainDataRaw.get("locoNumber");
        trainNumber = (String) driverTrainDataRaw.get("trainNumber");
        driverID = (String) driverTrainDataRaw.get("driverID");
        speedLimitKMPH = Utility.longToInt(driverTrainDataRaw.get("speedLimitKMPH"));
        wheelDiameter = Utility.longToInt(driverTrainDataRaw.get("wheelDiameter"));

        setTotalTime((String) driverTrainDataRaw.get("totalTime"));
        setTotalDistance(Utility.longToInt(driverTrainDataRaw.get("totalDistance")));
        setNumberOfOverSpeed(Utility.longToInt(driverTrainDataRaw.get("numberOfOverSpeed")));
        setMaxSpeed(Utility.longToInt(driverTrainDataRaw.get("maxSpeed")));
    }

    public String getLocoNumber() {
        return locoNumber;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public String getDriverID() {
        return driverID;
    }

    public int getSpeedLimitKMPH() {
        return speedLimitKMPH;
    }

    public int getWheelDiameter() {
        return wheelDiameter;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public int getNumberOfOverSpeed() {
        return numberOfOverSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public OperatorData getOperator() {
        return operator;
    }

    public void setOperator(OperatorData operator) {
        this.operator = operator;

        driverID = operator.getDriverID();
        trainNumber = operator.getTrainNumber();
        locoNumber = operator.getLocoNumber();
        speedLimitKMPH = operator.getSpeedLimitKMPH();
        wheelDiameter = operator.getWheelDiameter();
    }

    public void setTotalDistance(int totalDistance) {
        this.totalDistance = totalDistance;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public void setNumberOfOverSpeed(int numberOfOverSpeed) {
        this.numberOfOverSpeed = numberOfOverSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }
}
