package com.mounts.dataanalysis.backend.data;

import com.mounts.dataanalysis.backend.Utility;
import com.mounts.dataanalysis.backend.annotations.Config;
import org.joda.time.DateTime;
import org.json.simple.JSONObject;

@Config(length = 52, name = "MCFGRD.DAT")
public class OperatorData extends BaseData implements JSONSerializable{
    private String locoNumber;
    private String trainNumber;
    private String driverID;
    private String shedName;

    private int trainLoadTonnes;
    private int speedLimitKMPH;
    private int wheelDiameter;
    private int odoMeterKMS;
    private int locoType;

    public OperatorData() {}

    public OperatorData(byte[] data, String ypre) {
        super(data, 0, ypre);

        locoNumber = Utility.bytes2ascii(Utility.subArray(data, 6, 7));
        trainNumber = Utility.bytes2ascii(Utility.subArray(data, 13, 7));
        driverID = Utility.bytes2ascii(Utility.subArray(data, 20, 16));
        shedName = Utility.bytes2ascii(Utility.subArray(data, 46, 6));

        trainLoadTonnes = Utility.bytes2dec(Utility.subArray(data, 36, 2));
        speedLimitKMPH = Utility.byte2dec(data[38]);
        wheelDiameter = Utility.bytes2dec(Utility.subArray(data, 39, 2));
        odoMeterKMS = Utility.bytes2dec(Utility.subArray(data, 41, 4));
        locoType = Utility.byte2dec(data[45]);
    }

    public String getLocoNumber() {
        return locoNumber;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public String getDriverID() {
        return driverID;
    }

    public String getShedName() {
        return shedName;
    }

    public int getTrainLoadTonnes() {
        return trainLoadTonnes;
    }

    public int getSpeedLimitKMPH() {
        return speedLimitKMPH;
    }

    public int getWheelDiameter() {
        return wheelDiameter;
    }

    public int getOdoMeterKMS() {
        return odoMeterKMS;
    }

    public int getLocoType() {
        return locoType;
    }

    @SuppressWarnings("unchecked")
    public JSONObject toJSONObject() {
        JSONObject json = new JSONObject();

        json.put("locoNumber", locoNumber);
        json.put("trainNumber", trainNumber);
        json.put("driverID", driverID);
        json.put("shedName", shedName);
        json.put("trainLoadTonnes", trainLoadTonnes);
        json.put("speedLimitKMPH", speedLimitKMPH);
        json.put("wheelDiameter", wheelDiameter);
        json.put("odoMeterKMS", odoMeterKMS);
        json.put("locoType", locoType);

        JSONObject dateTime = new JSONObject();
        dateTime.put("start", getDateTimeMills());

        json.put("dateTime", dateTime);

        return json;
    }

    @Override
    public void fromJSONObject(JSONObject json) {
        locoNumber = (String) json.get("locoNumber");
        trainNumber = (String) json.get("trainNumber");
        driverID = (String) json.get("driverID");
        shedName = (String) json.get("shedName");
        trainLoadTonnes = Utility.longToInt(json.get("trainLoadTonnes"));
        speedLimitKMPH = Utility.longToInt(json.get("speedLimitKMPH"));
        wheelDiameter = Utility.longToInt(json.get("wheelDiameter"));
        odoMeterKMS = Utility.longToInt(json.get("odoMeterKMS"));
        locoType = Utility.longToInt(json.get("locoType"));

        dateTime = new DateTime(((JSONObject) json.get("dateTime")).get("start"));
    }
}
