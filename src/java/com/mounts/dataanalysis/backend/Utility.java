package com.mounts.dataanalysis.backend;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class Utility {
    public static int hex2dec(String hex) {
        return Integer.parseInt(hex, 16);
    }

    public static String bytes2ascii(byte[] b) {
        try {
            return new String(b, "ASCII");
        } catch (UnsupportedEncodingException e) {
            return "ENC_ERR";
        }
    }

    public static int bytes2dec(byte[] b) {
        StringBuilder hex = new StringBuilder();
        for(byte a: b) {
            hex.append(byte2hex(a));
        }

        return hex2dec(hex.toString());
    }

    public static int[] bytes2ints(byte[] b) {
        int[] i = new int[b.length];
        int j = 0;
        for(byte a: b) {
            i[j++] = byte2dec(a);
        }

        return i;
    }

    public static String byte2bin(byte b) {
        return String.format("%8s", Integer.toBinaryString(byte2dec(b))).replace(' ', '0');
    }

    public static String byte2hex(byte b) {
        return String.format("%02X", b);
    }

    public static int byte2dec(byte b) {
        return b & 0xFF;
    }

    public static byte[] subArray(byte[] b, int start, int length) {
        return Arrays.copyOfRange(b, start, start + length);
    }

    public static DateTime parseDate(byte[] data, String ypre) {
        byte year = data[2],
            month = data[1],
            day = data[0],
            hour = data[3],
            minute = data[4],
            second = data[5];
        String date = String.format(ypre + "%02d-%02d-%02dT%02d:%02d:%02d", year, month, day, hour, minute, second);

        return DateTime.parse(date);
    }

    public static DateTime javaDate2JodaDate(LocalDate ld) {
        LocalDateTime ldt = LocalDateTime.of(ld, LocalTime.of(0,0));
        return new DateTime(ldt.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
    }

    public static DateTime javaDate2JodaDate(LocalDate ld, LocalTime lt) {
        LocalDateTime ldt = LocalDateTime.of(ld, lt);
        return new DateTime(ldt.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
    }

    public static String toHMS(long instance) {
        int seconds = (int) (instance / 1000) % 60 ;
        int minutes = (int) ((instance / (1000*60)) % 60);
        int hours   = (int) ((instance / (1000*60*60)) % 24);

        return hours + ":" + minutes + ":" + seconds;
    }

    public static String findFile(String path, String name) {
        File root = new File(path + "/");
        Collection files = FileUtils.listFiles(root, new String[] {"DAT"}, true);

        for(Iterator iterator = files.iterator(); iterator.hasNext();) {
            File file = (File) iterator.next();
            String fileName = file.getName();
            fileName = fileName.substring(fileName.lastIndexOf('\\') + 1);

            if (fileName.equals(name))
                return file.getAbsolutePath();
        }

        return "";
    }

    public static int longToInt(Object l) {
        return ((Long) l).intValue();
    }

    //adding by chann
    public static double longToDouble(Object l){//writing by chann
        return (double) l;
    }
}
