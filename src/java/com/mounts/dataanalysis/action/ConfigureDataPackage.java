package com.mounts.dataanalysis.action;

import com.mounts.dataanalysis.backend.datapackage.DataPackageConfig;
import com.mounts.dataanalysis.backend.data.*;

public class ConfigureDataPackage extends BaseAction<String, Void> {
    private DataPackageConfig config;

    public ConfigureDataPackage(DataPackageConfig _config) {
        config = _config;
    }

    @Override
    public Void run(String... toggle) {
        switch (toggle[0]) {
            case "STM":
                config.generateDTW();
                config.addDataClass(ShortTerm.class);
                config.addDataClass(OperatorData.class);
                config.addDataClass(MainConfig.class);
                config.addDataClass(ERRLog.class);
                break;
            case "LTM":
                config.generateDTW();
                config.addDataClass(LongTerm.class);
                config.addDataClass(OperatorData.class);
                config.addDataClass(MainConfig.class);
                config.addDataClass(ERRLog.class);
                break;
            case "MCM":
                config.addDataClass(MainConfig.class);
                break;
            case "ELM":
                config.addDataClass(ERRLog.class);
                break;
        }

        return null;
    }
}
