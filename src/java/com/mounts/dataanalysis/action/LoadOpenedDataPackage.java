package com.mounts.dataanalysis.action;

import com.mounts.dataanalysis.Log;
import com.mounts.dataanalysis.backend.datapackage.DataPackage;
import com.mounts.dataanalysis.controllers.MainController;
import com.mounts.dataanalysis.controllers.tables.*;

public class LoadOpenedDataPackage extends BaseAction<DataPackage, Void> {
    public static String TAG = LoadOpenedDataPackage.class.getSimpleName();

    private MainController controller;

    public LoadOpenedDataPackage(MainController _controller) {
        controller = _controller;
    }

    @Override
    public Void run(DataPackage... params) {
        DataPackage dataPackage = params[0];

        try {
            dataPackage.generateDTR();
            controller.addTable(ShortLongTermTableController.class, 0, dataPackage.getShortTermDataList());//ShortTerm
            controller.addTable(ShortLongTermTableController.class, 1, dataPackage.getLongTermDataList());//LongTerm
            controller.addTable(OperatorDataTableController.class, 2, dataPackage.getOperatorDataList());
            controller.addTable(DTRTableController.class, 3, dataPackage.getDriverTrainWiseDataList());
            controller.addTable(HTDTableController.class, 4, dataPackage.getHaltTimeDataList());
            controller.addTable(MainConfigTableController.class, 5, dataPackage.getMainConfigDataList());
            controller.addTable(ErrorLogTableController.class, 6, dataPackage.getErrLogDataList());
        } catch (Exception e) {
            Log.debug(TAG, "run");
            e.printStackTrace();
        }

        return null;
    }
}
