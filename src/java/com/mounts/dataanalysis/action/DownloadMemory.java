package com.mounts.dataanalysis.action;

import com.mounts.dataanalysis.backend.datapackage.DataPackageConfig;
import com.mounts.dataanalysis.controllers.DownloadMemoryController;
import com.mounts.dataanalysis.task.DownloadTask;

public class DownloadMemory extends BaseAction<String, Void> {
    private DownloadMemoryController controller;
    private String path;
    private DataPackageConfig config;

    public DownloadMemory(DownloadMemoryController _controller, String _path) {
        controller = _controller;
        path = _path;
        config = new DataPackageConfig(false);
    }

    @Override
    public Void run(String... params) {
        new ConfigureDataPackage(config).run(params[1]);
        new DownloadTask(controller, path, config).execute(params[0], params[1]);
        return null;
    }
}
