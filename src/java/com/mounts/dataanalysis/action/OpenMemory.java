package com.mounts.dataanalysis.action;

import com.mounts.dataanalysis.controllers.OpenMemoryController;
import com.mounts.dataanalysis.task.OpenTask;

public class OpenMemory extends BaseAction<String, Void> {
    private OpenMemoryController controller;

    public OpenMemory(OpenMemoryController _controller) {
        controller = _controller;
    }

    @Override
    public Void run(String... params) {
        new OpenTask(controller, params[0]).execute();
        return null;
    }
}
