package com.mounts.dataanalysis.action;

abstract public class BaseAction<T1, T2> {
    public abstract T2 run(T1... params);
}
