package com.mounts.dataanalysis.action;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

public class IncrementHashColumn extends BaseAction<TableColumn<?, ?>, Void> {
    @Override
    @SuppressWarnings("unchecked")
    public Void run(TableColumn<?, ?>... params) {
        TableColumn numberColumn = params[0];

        numberColumn.setCellFactory(new Callback<javafx.scene.control.TableColumn, TableCell>() {
            @Override public TableCell call(javafx.scene.control.TableColumn param) {
                return new TableCell() {
                    @Override protected void updateItem(Object item, boolean empty) {
                        if (!empty && this.getTableRow() != null) {
                            setText(this.getTableRow().getIndex() + 1 + "");
                        } else {
                            setText("");
                        }
                    }
                };
            }
        });

        numberColumn.setSortable(false);
        return null;
    }
}
