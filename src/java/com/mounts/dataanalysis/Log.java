package com.mounts.dataanalysis;

public abstract class Log {

    public static void debug(String... args) {
        StringBuilder logStringBuilder = new StringBuilder();

        for (String arg: args) {
            logStringBuilder.append(arg);
            logStringBuilder.append(" ");
        }

        System.err.println(logStringBuilder.toString());
    }
}
