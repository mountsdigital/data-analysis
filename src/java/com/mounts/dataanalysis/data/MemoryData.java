package com.mounts.dataanalysis.data;

public class MemoryData {
    private String name, type;
    private String date;
    private String fileName;

    public MemoryData(String memoryFileName) {
        String[] nameList = memoryFileName.split("-");

        fileName = memoryFileName;
        name = nameList[0];
        type = nameList[1];

        date = String.format("%d-%d-%d %d:%d:%d %s",
                Integer.parseInt(nameList[2]), Integer.parseInt(nameList[3]), Integer.parseInt(nameList[4]),
                Integer.parseInt(nameList[5]), Integer.parseInt(nameList[6]), Integer.parseInt(nameList[7]),
                nameList[8]
        );
    }

    public String getFileName() {
        return fileName;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}
