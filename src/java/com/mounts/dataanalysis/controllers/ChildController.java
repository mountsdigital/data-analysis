package com.mounts.dataanalysis.controllers;

import com.chan.jerk.Controller;
import com.chan.jerk.Layout;
import javafx.fxml.Initializable;
import javafx.scene.Parent;

import java.net.URL;
import java.util.ResourceBundle;

@Layout("ChildView.fxml")
public class ChildController extends Controller implements Initializable {
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @Override
    public void onLoaded(Parent root) {
        currentStage.setMaximized(true);
    }

    @Override
    public void onResume() {

    }
}