package com.mounts.dataanalysis.controllers;

import com.chan.jerk.Controller;
import com.chan.jerk.Layout;
import com.mounts.dataanalysis.action.IncrementHashColumn;
import com.mounts.dataanalysis.action.OpenMemory;
import com.mounts.dataanalysis.backend.datapackage.DataPackage;
import com.mounts.dataanalysis.data.MemoryData;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.util.Callback;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

@Layout("OpenMemoryView")
public class OpenMemoryController extends Controller implements Initializable {
    @FXML private BorderPane pane;
    @FXML private Text txtStatus;
    @FXML private TableView<MemoryData> tbShortTerm, tbLongTerm, tbMainConfig, tbErrLog;

    private ObservableList<MemoryData> tbShortTermData = FXCollections.observableArrayList();
    private ObservableList<MemoryData> tbLongTermData = FXCollections.observableArrayList();
    private ObservableList<MemoryData> tbMainConfigData = FXCollections.observableArrayList();
    private ObservableList<MemoryData> tbErrLogData = FXCollections.observableArrayList();

    private boolean isAborted = false;

    @SuppressWarnings("unchecked")
    private Callback rowFactory = memoryDataTableView -> {
        final TableView<MemoryData> tbv = (TableView<MemoryData>) memoryDataTableView;
        final TableRow<MemoryData> row = new TableRow<>();

        row.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2) {
                new OpenMemory(OpenMemoryController.this).run(tbv.getSelectionModel().getSelectedItem().getFileName());
            }
        });
        return row;
    };

    @Override
    @SuppressWarnings("unchecked")
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setHashCellFactory(tbShortTerm.getColumns().get(0));
        setHashCellFactory(tbLongTerm.getColumns().get(0));
        setHashCellFactory(tbMainConfig.getColumns().get(0));
        setHashCellFactory(tbErrLog.getColumns().get(0));

        tbShortTerm.setRowFactory(rowFactory);
        tbLongTerm.setRowFactory(rowFactory);
        tbMainConfig.setRowFactory(rowFactory);
        tbErrLog.setRowFactory(rowFactory);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onClose() {
        //TODO remove this process and make task cancelable
        if (pane.isDisabled()) {
            isAborted = true;
        }
    }

    @Override
    public void onLoaded(Parent root) {
        super.onLoaded(root);
        currentStage.setTitle("Open");
    }

    public void setStatusLoading(boolean statusLoading) {
        if (statusLoading) {
            pane.setDisable(true);
            txtStatus.setText("Status: Loading");
        } else {
            pane.setDisable(false);
            txtStatus.setText("Status: Idle");
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void passExtras(Object... extras) {
        ArrayList<MemoryData>[] data = (ArrayList<MemoryData>[]) extras[0];

        tbShortTermData.addAll(data[0]);
        tbLongTermData.addAll(data[1]);
        tbMainConfigData.addAll(data[2]);
        tbErrLogData.addAll(data[3]);

        tbShortTerm.setItems(tbShortTermData);
        tbLongTerm.setItems(tbLongTermData);
        tbMainConfig.setItems(tbMainConfigData);
        tbErrLog.setItems(tbErrLogData);
    }

    @SuppressWarnings("unchecked")
    public void setOpenResult(DataPackage dataPackage) {
        if (!isAborted)
            result.onResult(dataPackage);
        currentStage.close();
    }

    @SuppressWarnings("unchecked")
    private void setHashCellFactory(TableColumn numberColumn) {
        new IncrementHashColumn().run(numberColumn);
    }
}
