package com.mounts.dataanalysis.controllers;

import com.chan.jerk.Controller;
import com.chan.jerk.Jerk;
import com.chan.jerk.Layout;
import com.mounts.dataanalysis.action.LoadOpenedDataPackage;
import com.mounts.dataanalysis.backend.datapackage.DataPackage;
import com.mounts.dataanalysis.controllers.tables.TableControllerInterface;
import com.mounts.dataanalysis.task.DownloadTask;
import com.mounts.dataanalysis.task.LoadFilesTask;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.TabPane;
import javafx.scene.text.Text;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

@Layout("MainView")
public class MainController extends Controller {
    @FXML
    private TabPane tabPane;
    @FXML
    private Text txtStatus;

    @Override
    public void onLoaded(Parent root) {
        super.onLoaded(root);

        currentStage.setMaximized(true);
        currentStage.setTitle("Train Data Analysis");
    }

    @Override
    public void onResume() {
    }

    public void setStatus(String status) {
        if (status.isEmpty())
            txtStatus.setText(status);
        else txtStatus.setText("Status: " + status);
    }

    @SuppressWarnings("unchecked")
    public void addTable(Class klass, int index, ArrayList<?> data) throws IOException {

        TableControllerInterface controller = (TableControllerInterface) new Jerk(klass)
                .startIn(tabPane.getTabs().get(index), (parent, child) -> {


                    parent.setContent(child);
                });

        controller.setData(data);

    }

    public void btnOpen_OnAction() {
        new LoadFilesTask(this, currentStage, result -> {
            new LoadOpenedDataPackage(this).run((DataPackage) result);
        }).execute();


    }

    public void btnDownloadData_onAction() throws IOException {
        Jerk downloadEngine = new Jerk(DownloadMemoryController.class);
        downloadEngine.makeModelWindow(currentStage);

        downloadEngine.start(result -> {
            DownloadTask.DownloadTaskResult taskResult = (DownloadTask.DownloadTaskResult) result;

            if (taskResult.isAborted()) return;

            if (!taskResult.isSuccess()) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setHeaderText(null);
                alert.setContentText("Error occurs while downloading.");

                alert.showAndWait();
            } else {
                new LoadOpenedDataPackage(this).run(taskResult.getDataPackage());
            }
        }).show();
    }

    public void btnPrint_OnAction(ActionEvent actionEvent) throws IOException {




        try {
            Desktop desktop = null;
            if (Desktop.isDesktopSupported()) {
                desktop = Desktop.getDesktop();
            }

            desktop.print(new File("C:\\mkt\\workbook.xls"));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }


    }

}