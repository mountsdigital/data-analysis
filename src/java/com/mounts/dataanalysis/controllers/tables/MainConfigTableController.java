package com.mounts.dataanalysis.controllers.tables;

import com.chan.jerk.Layout;
import com.mounts.dataanalysis.backend.data.MainConfig;
import com.mounts.dataanalysis.backend.data.ShortTerm;

@Layout("TableMainConfig")
public class MainConfigTableController extends GenericTableController<MainConfig> {

}
