package com.mounts.dataanalysis.controllers.tables;

import com.chan.jerk.Layout;
import com.mounts.dataanalysis.backend.data.ERRLog;
import com.mounts.dataanalysis.backend.data.ShortTerm;

@Layout("TableErrorLog")
public class ErrorLogTableController extends GenericTableController<ERRLog> {

}
