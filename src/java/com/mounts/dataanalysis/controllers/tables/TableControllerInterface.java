package com.mounts.dataanalysis.controllers.tables;

import java.util.ArrayList;

public interface TableControllerInterface {
    void setData(ArrayList<?> data);
}
