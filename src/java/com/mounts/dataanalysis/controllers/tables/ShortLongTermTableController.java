package com.mounts.dataanalysis.controllers.tables;

import com.chan.jerk.Layout;
import com.mounts.dataanalysis.backend.data.DriverTrainWiseData;
import com.mounts.dataanalysis.backend.data.ShortTerm;
import com.mounts.dataanalysis.backend.datapackage.DataPackage;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


@Layout("TableShortLongTerm")
public class ShortLongTermTableController extends GenericTableController<ShortTerm> {


    @FXML
    public Label labelLocoNumber;

    @FXML
    public Label labelTrainNumber;

    @FXML
    public Label labelDriverID;

    @FXML
    public Label labelLoad;

    @FXML
    public Label labelSpeedLimit;

    @FXML
    public TextField txfSpeedScale;

    @FXML
    public TextField txfDistanceScale;

    @FXML
    public TextField txfSpeedFilter;

    @FXML
    public TextField txfTimeFilter;


    private double scale;
    ObservableList<ShortTerm> shortTermItems = FXCollections.observableArrayList();
    ObservableList<ShortTerm> selectedItems;

    DataPackage dataPackage = new DataPackage();
    private static int lastCount=0;
    private String nextDate,previousDate,currentDate;
    private int count=0;


    @Override
    public void onResume() {
        super.onResume();

        labelLocoNumber.setText("");
    }


    @Override
    protected void onTableViewReady() {


        tableView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );

        ArrayList<ShortTerm> shortTerms = new ArrayList<>(tableView.getItems());
        shortTermItems.addAll(shortTerms);

//multiselection action event on tableview
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                selectedItems = tableView.getSelectionModel().getSelectedItems();
                System.out.println("Selected item size" + selectedItems.size());

                Workbook workbook = new HSSFWorkbook();
//                workbook.setHidden(true);
                Sheet spreadsheet = workbook.createSheet("train");
//                spreadsheet.setColumnHidden(1,true);
                Row row = spreadsheet.createRow(0);

                for (int j = 0; j < tableView.getColumns().size(); j++) {
                    row.createCell(j).setCellValue(tableView.getColumns().get(j).getText());
                }

                for (int i = 0; i < selectedItems.size(); i++) {

                    row = spreadsheet.createRow(i + 1);
                    row.createCell(0).setCellValue(selectedItems.get(i).getDate());
                    row.createCell(1).setCellValue(selectedItems.get(i).getTime());
                    row.createCell(2).setCellValue(selectedItems.get(i).getSpeed());
                    row.createCell(3).setCellValue(selectedItems.get(i).getDistance());
                    row.createCell(4).setCellValue(selectedItems.get(i).getTotalDisKm());
                    row.createCell(5).setCellValue(selectedItems.get(i).getBreakPressure());
                    row.createCell(6).setCellValue(selectedItems.get(i).getEventOne());
                    row.createCell(7).setCellValue(selectedItems.get(i).getEventTwo());


//                        else {
//                            row.createCell(j).setCellValue("");
//                        }

                }

                FileOutputStream fileOut = null;

                try {
                    fileOut = new FileOutputStream("C:\\mkt\\workbook.xls");

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    workbook.write(fileOut);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    fileOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });


        tableView.setRowFactory(tv -> {
            TableRow<ShortTerm> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                ShortTerm item = row.getItem();
                DriverTrainWiseData dtr = item.getDriverTrainWiseData();

                if (!row.isEmpty() && dtr != null) {
                    labelLocoNumber.setText(dtr.getLocoNumber());

                    labelTrainNumber.setText(dtr.getTrainNumber());

                    labelDriverID.setText(dtr.getDriverID());

                    labelLoad.setText(dtr.getOperator().getTrainLoadTonnes() + "");

                    labelSpeedLimit.setText(dtr.getSpeedLimitKMPH() + "");
                }
            });

            return row;
        });
    }


    @FXML
    public void btnTimeSpeed_OnAction(MouseEvent mouseEvent) {


        if (txfSpeedScale.getText().isEmpty()) return;

        else {
            System.out.println("SpeedScale" + txfSpeedScale.getText());

            scale = Integer.parseInt(txfSpeedScale.getText());

            Slider slider = new Slider();
            slider.setMin(0);
            slider.setMax(shortTermItems.size()- 100);
            slider.setValue(0);
            slider.setOrientation(Orientation.HORIZONTAL);
            slider.setBlockIncrement(1);

            slider.setPadding(new Insets(0, 20, 0, 60));

//        slider.setMin(0);
//        slider.setMax(100);
//        slider.setValue(0);
//
//        slider.setShowTickLabels(true);
//        slider.setShowTickMarks(true);
//
//        slider.setBlockIncrement(10);
//
//        // Adding Listener to value property.
//        slider.valueProperty().addListener(new ChangeListener<Number>() {
//
//            @Override
//            public void changed(ObservableValue<? extends Number> observable, //
//                                Number oldValue, Number newValue) {
//
//                infoLabel.setText("New value: " + newValue);
//            }
//        });


            VBox root = new VBox();
            root.setSpacing(25);

            CategoryAxis xAxis = new CategoryAxis();
            xAxis.setLabel("Time");
            xAxis.setPrefWidth(350);

            NumberAxis yAxis = new NumberAxis(0, 200, 20);
            yAxis.setLabel("Speed in Km");
            yAxis.setMinorTickVisible(false);


            LineChart<String, Number> lineChart = new LineChart<String, Number>(xAxis, yAxis);
            lineChart.setTitle("Speed per Time Graph");
            lineChart.setMinSize(1500, 500);
            lineChart.setPrefSize(shortTermItems.size(), 500);
            Series<String, Number> series = new Series<>();
            series.setName("Train Data Analysis");


            Text startDate = new Text(" Start Date:----,                             Start Time:----, \n End Date:----,                               End Time:----, \n Loco Number:----,                        Train Number:----,              Driver Id:----, \n Speed Limit KMPH:----,                 Wheel Diameter:----");

//            Text endDate = new Text("End Date:----");
//            Text startTime=new Text("Start Time:----");
//            Text endTime=new Text("End Time:----");
//            Text locaNumber=new Text("Loco Number:----");
//            Text trainNumber=new Text("Train Number:----");
//            Text driverId=new Text("Driver Id:----");
//            Text speedLimitKMPH=new Text("Speed Limit KMPH:----");
//            Text wheelDia=new Text("Wheel Diameter:----");

            lineChart.getData().add(series);


            for (int i = 0; i < 100; i++) {

                Data<String, Number> data = new Data<String, Number>(shortTermItems.get(i).getTime(), shortTermItems.get(i).getSpeed());

                series.getData().add(data);

                data.getNode().setOnMouseClicked(event -> {
                    String time = data.getXValue();
                    int speed = data.getYValue().intValue();

                    for(int j=0;j<100;j++){
                         if(shortTermItems.get(j).getTime().equals(time) && shortTermItems.get(j).getSpeed()==speed){
                             ShortTerm item = shortTermItems.get(j);
                             DriverTrainWiseData dtr = item.getDriverTrainWiseData();

                             if (dtr != null) {

                                 String text="Start Date:"+dtr.getDate()+ "                    Start Time:"+dtr.getTime() +"\nEnd Date:"+dtr.getDateEnd()+ "                      End Time:"+dtr.getTimeEnd() +"\nLoca Number:"+dtr.getLocoNumber()+ "                  Train Number:"+dtr.getTrainNumber() +"               Driver Id:"+dtr.getDriverID() +"\nSpeedLimitKMPH:"+dtr.getSpeedLimitKMPH()+ "                   Wheel Diameter:"+dtr.getWheelDiameter();
                                 startDate.setText(text);
//                                 startTime.setText("Start Time:"+dtr.getTime());
//
//                                 endDate.setText("End Date:"+dtr.getDateEnd());
//                                 endTime.setText("End Time:"+dtr.getTimeEnd());
//
//                                 locaNumber.setText("Loca Number:"+dtr.getLocoNumber());
//                                 trainNumber.setText("Train Number:"+dtr.getTrainNumber());
//
//                                 driverId.setText("Driver Id:"+dtr.getDriverID());
//                                 speedLimitKMPH.setText("SpeedLimitKMPH:"+dtr.getSpeedLimitKMPH());
//
//                                 wheelDia.setText("Wheel Diameter:"+dtr.getWheelDiameter());

                             }
                         }
                    }

                });
            }

            slider.valueProperty().addListener((observable, oldValue, newValue) -> {

                series.getData().clear();

                for (int count = newValue.intValue(); count < newValue.intValue()+100; count++) {
                    Data<String, Number> data = new Data<String, Number>(shortTermItems.get(count).getTime(), shortTermItems.get(count).getSpeed());
                    series.getData().add(data);

                    data.getNode().setOnMouseClicked(event -> {
                        String time = data.getXValue();
                        int speed = data.getYValue().intValue();

                        for(int j=newValue.intValue();j<newValue.intValue()+100;j++){
                            if(shortTermItems.get(j).getTime().equals(time) && shortTermItems.get(j).getSpeed()==speed){
                                ShortTerm item = shortTermItems.get(j);
                                DriverTrainWiseData dtr = item.getDriverTrainWiseData();

                                if (dtr != null) {
                                    String text="Start Date:"+dtr.getDate()+ "                    Start Time:"+dtr.getTime() +"\nEnd Date:"+dtr.getDateEnd()+ "                      End Time:"+dtr.getTimeEnd() +"\nLoca Number:"+dtr.getLocoNumber()+ "                  Train Number:"+dtr.getTrainNumber() +"               Driver Id:"+dtr.getDriverID() +"\nSpeedLimitKMPH:"+dtr.getSpeedLimitKMPH()+ "                   Wheel Diameter:"+dtr.getWheelDiameter();
                                    startDate.setText(text);
//                                    startTime.setText("Start Time:"+dtr.getTime());
//
//                                    endDate.setText("End Date:"+dtr.getDateEnd());
//                                    endTime.setText("End Time:"+dtr.getTimeEnd());
//
//                                    locaNumber.setText("Loca Number:"+dtr.getLocoNumber());
//                                    trainNumber.setText("Train Number:"+dtr.getTrainNumber());
//
//                                    driverId.setText("Driver Id:"+dtr.getDriverID());
//                                    speedLimitKMPH.setText("SpeedLimitKMPH:"+dtr.getSpeedLimitKMPH());
//
//                                    wheelDia.setText("Wheel Diameter:"+dtr.getWheelDiameter());

                                }
                            }
                        }

                    });


                }

            });


//            lineChart.setCreateSymbols(true);
            root.getChildren().add(lineChart);
            root.getChildren().add(slider);

            root.getChildren().addAll(startDate);

            Button btnImagePrint = new Button("Print Image");


            btnImagePrint.setAlignment(Pos.BOTTOM_CENTER);
//            root.getChildren().add(startDate);
//            root.getChildren().add(endDate);
            root.getChildren().add(btnImagePrint);
            root.setAlignment(Pos.CENTER);


            Label label = new Label("Train Analysis Graph Chart");

            Scene secondScene = new Scene(root, 1500, 900);
            Stage secondStage = new Stage();
            secondStage.setScene(secondScene); // set the scene
            secondStage.setTitle("Train Analysis Graph Chart");

            btnImagePrint.setOnAction((ActionEvent event) -> {
                System.out.println("Speed per Time Graph action");
                captureImage(secondScene);
                try {
                    Desktop desktop = null;
                    if (Desktop.isDesktopSupported()) {
                        desktop = Desktop.getDesktop();
                    }

                    desktop.print(new File("C:\\mkt\\chart.png"));
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            });

            secondStage.show();

        }

    }

    public void btnTimeDistance_OnAction(MouseEvent mouseEvent) {

        if (txfDistanceScale.getText().isEmpty()) return;

        else {


            scale = Double.parseDouble(txfDistanceScale.getText());
            System.out.println("scale:"+scale);


            Series<Number, Number> series = new Series<>();
            series.setName("Train Data Analysis");

            Slider slider = new Slider();
            slider.setMin(0);
            slider.setMax(shortTermItems.size()-500);
            slider.setOrientation(Orientation.HORIZONTAL);
            slider.setBlockIncrement(1);
            slider.setPadding(new Insets(0, 20, 0, 60));
            StringConverter<Double> stringConverter=new StringConverter<Double>() {
                @Override
                public String toString(Double object) {

                    return shortTermItems.get(object.intValue()).getTime()+"\n"+shortTermItems.get(object.intValue()).getDate();
                }

                @Override
                public Double fromString(String string) {
                    return null;
                }
            };

            slider.setLabelFormatter(stringConverter);
            slider.setMajorTickUnit(10000);
            slider.setShowTickLabels(true);


            VBox root = new VBox();
            HBox hBox=new HBox();

            Text startDate = new Text(" Start Date:----,                             Start Time:----, \n End Date:----,                               End Time:----, \n Loco Number:----,                        Train Number:----,              Driver Id:----, \n Speed Limit KMPH:----,                 Wheel Diameter:----");


            Button previous=new Button("Previous");
            Button next=new Button("Next");
            hBox.getChildren().add(previous);
            hBox.getChildren().add(next);
            root.setSpacing(25);

            NumberAxis xAxis = new NumberAxis(shortTermItems.get(0).getTotalDisKm(),shortTermItems.get(500).getTotalDisKm(),scale+0.00001+shortTermItems.get(0).getTotalDisKm());
            xAxis.setTickUnit(scale);
            xAxis.setMinorTickVisible(false);
            NumberAxis yAxis = new NumberAxis(0,200,20);
            yAxis.setMinorTickVisible(false);


            LineChart<Number, Number> lineChart = new LineChart<Number, Number>(xAxis, yAxis);
            lineChart.setMinSize(1500, 500);
            lineChart.setPrefSize(shortTermItems.size(), 500);
            lineChart.getData().add(series);
            lineChart.setTitle("Total Distance Km per time");
            series.setName("Train Data Analysis");
//            Text currentDate = new Text("Current Date:" + dateList.get(0));

//            nextDate=shortTermItems.get(0).getDate();
//            previousDate=shortTermItems.get(0).getDate();
//            currentDate=shortTermItems.get(0).getDate();
//
//            while (shortTermItems.get(count).getDate().equals(currentDate)){
//                Data<Number,Number> data=new Data<Number, Number>(shortTermItems.get(count).getTotalDisKm(), shortTermItems.get(count).getSpeed());
//                series.getData().add(data);
//                count++;
//                System.out.println("countfor:"+count);
//            }
//            if(count<shortTermItems.size()){
//                nextDate=shortTermItems.get(count).getDate();
//                next.setText(nextDate+">>");
//            }

//            next.setOnAction(new EventHandler<ActionEvent>() {
//                @Override
//                public void handle(ActionEvent event) {
//                    previous.setText(currentDate);
//                    while (shortTermItems.get(count).getDate().equals(nextDate)){
//                        Data<Number,Number> data=new Data<Number, Number>(shortTermItems.get(count).getTotalDisKm(), shortTermItems.get(count).getSpeed());
//                        series.getData().add(data);
//                        count++;
//                    }
//                    if(count<shortTermItems.size()){
//                        nextDate=shortTermItems.get(count).getDate();
//                        next.setText(nextDate+">>");
//                    }
//                }
//            });

            for (int i = 0; i <500; i++) {

                Data<Number,Number> data=new Data<Number, Number>(shortTermItems.get(i).getTotalDisKm(), shortTermItems.get(i).getSpeed());
                series.getData().add(data);

                data.getNode().setOnMouseClicked(event -> {

                    double totalKm = (double) data.getXValue();
                    int speed = data.getYValue().intValue();

                    for(int j=0;j<500;j++){
                        if(shortTermItems.get(j).getTotalDisKm()==totalKm && Math.abs(speed-shortTermItems.get(j).getSpeed())<0.0001){
                            ShortTerm item = shortTermItems.get(j);
                            DriverTrainWiseData dtr = item.getDriverTrainWiseData();

                            if (dtr != null) {
                                String text="Start Date:"+dtr.getDate()+ "                    Start Time:"+dtr.getTime() +"\nEnd Date:"+dtr.getDateEnd()+ "                      End Time:"+dtr.getTimeEnd() +"\nLoca Number:"+dtr.getLocoNumber()+ "                  Train Number:"+dtr.getTrainNumber() +"               Driver Id:"+dtr.getDriverID() +"\nSpeedLimitKMPH:"+dtr.getSpeedLimitKMPH()+ "                   Wheel Diameter:"+dtr.getWheelDiameter();
                                startDate.setText(text);
                            }
                        }
                    }


                });
            }

            slider.valueProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {


                    series.getData().clear();

                    xAxis.setLowerBound(shortTermItems.get(newValue.intValue()).getTotalDisKm());
                    xAxis.setUpperBound(shortTermItems.get(newValue.intValue()+500).getTotalDisKm());
                    xAxis.setTickUnit(scale+shortTermItems.get(0).getTotalDisKm());

                    System.out.println("newValue:"+newValue.intValue());


                    for (int count = newValue.intValue(); count < newValue.intValue() + 500; count++) {
                        Data<Number,Number> data=new Data<Number, Number>(shortTermItems.get(count).getTotalDisKm(),shortTermItems.get(count).getSpeed());
                        series.getData().add(data);

                        data.getNode().setOnMouseClicked(event -> {

                            double totalKm = (double) data.getXValue();
                            int speed = data.getYValue().intValue();

                            for(int j=newValue.intValue();j<500+newValue.intValue();j++){
                                if(shortTermItems.get(j).getTotalDisKm()==totalKm && Math.abs(speed-shortTermItems.get(j).getSpeed())<0.0001){
                                    ShortTerm item = shortTermItems.get(j);
                                    DriverTrainWiseData dtr = item.getDriverTrainWiseData();

                                    if (dtr != null) {
                                        String text="Start Date:"+dtr.getDate()+ "                    Start Time:"+dtr.getTime() +"\nEnd Date:"+dtr.getDateEnd()+ "                      End Time:"+dtr.getTimeEnd() +"\nLoca Number:"+dtr.getLocoNumber()+ "                  Train Number:"+dtr.getTrainNumber() +"               Driver Id:"+dtr.getDriverID() +"\nSpeedLimitKMPH:"+dtr.getSpeedLimitKMPH()+ "                   Wheel Diameter:"+dtr.getWheelDiameter();
                                        startDate.setText(text);
                                    }
                                }
                            }


                        });
                    }

                }
            });




//            lineChart.setCreateSymbols(true);
            root.getChildren().add(lineChart);
//            root.getChildren().add(hBox);
            root.getChildren().add(slider);

            Button btnImagePrint = new Button("Print Image");



            btnImagePrint.setAlignment(Pos.BOTTOM_CENTER);
            root.getChildren().add(startDate);
            root.getChildren().add(btnImagePrint);
            root.setAlignment(Pos.CENTER);


            Label label = new Label("Train Analysis Graph Chart");

            Scene secondScene = new Scene(root, 1500, 900);
            Stage secondStage = new Stage();
            secondStage.setScene(secondScene); // set the scene
            secondStage.setTitle("Train Analysis Graph Chart");

            btnImagePrint.setOnAction((ActionEvent event) -> {
                System.out.println("Distance per Time Graph action");
                captureImage(secondScene);
                try {
                    Desktop desktop = null;
                    if (Desktop.isDesktopSupported()) {
                        desktop = Desktop.getDesktop();
                    }

                    desktop.print(new File("C:\\mkt\\chart.png"));
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            });

            secondStage.show();

        }
    }

    private void captureImage(Scene secondScene) {

        WritableImage image = secondScene.snapshot(new WritableImage(1500, 700));


        File file = new File("C:\\mkt\\chart.png");

        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLoaded(Parent root) {
        super.onLoaded(root);
    }

    @Override
    public void setData(ArrayList<?> data) {
        super.setData(data);
    }

    public void btnSpeedFilter_OnAction(MouseEvent mouseEvent) {

        if (txfSpeedFilter.getText().isEmpty()) {
            return;
        } else {
            int speed = Integer.parseInt(txfSpeedFilter.getText());
            System.out.println("Filter Speed:" + speed);

            ArrayList<ShortTerm> shortTerms = new ArrayList<>();

            ObservableList<ShortTerm> items = FXCollections.observableArrayList();

            for (int i = 0; i < tableView.getItems().size(); i++) {

                if (tableView.getItems().get(i).getSpeed() >= speed)

                    shortTerms.add(tableView.getItems().get(i));

            }

            items.addAll(shortTerms);


            tableView.setItems(items);
            tableView.refresh();


        }


    }


    public void btnTimeFilter_OnAction(MouseEvent mouseEvent) {

        if (txfTimeFilter.getText().isEmpty()) {
            return;
        } else {
            String time = txfTimeFilter.getText();
            ArrayList<ShortTerm> shortTerms = new ArrayList<>();

            ObservableList<ShortTerm> items = FXCollections.observableArrayList();

            for (int i = 0; i < tableView.getItems().size(); i++) {

                if (tableView.getItems().get(i).getTime().equals(time))

                    shortTerms.add(tableView.getItems().get(i));

            }

            items.addAll(shortTerms);


            tableView.setItems(items);
            tableView.refresh();
        }
    }

    public void btnClearFilter_OnAction(MouseEvent mouseEvent) {

        tableView.setItems(shortTermItems);
        tableView.refresh();
    }

}

