package com.mounts.dataanalysis.controllers.tables;

import com.chan.jerk.Layout;
import com.mounts.dataanalysis.backend.data.DriverTrainWiseData;
import com.mounts.dataanalysis.backend.data.ShortTerm;

@Layout("TableDTR")
public class DTRTableController extends GenericTableController<DriverTrainWiseData> {

}
