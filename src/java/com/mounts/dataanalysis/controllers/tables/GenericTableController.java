package com.mounts.dataanalysis.controllers.tables;

import com.chan.jerk.Controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;

import java.util.ArrayList;

public class GenericTableController<T> extends Controller implements TableControllerInterface {
    @FXML  protected TableView<T> tableView;

    protected ObservableList<T> tableData = FXCollections.observableArrayList();

    @Override
    @SuppressWarnings("unchecked")
    public void setData(ArrayList<?> data) {
        if (data != null && data.size() > 0)
            tableData.addAll((ArrayList<T>) data);

        tableView.setItems(tableData);

        onTableViewReady();
    }

    @Override
    public void onResume() {}

    protected void onTableViewReady() {}

    public TableView<T> getTableView() {
        return tableView;
    }

}
