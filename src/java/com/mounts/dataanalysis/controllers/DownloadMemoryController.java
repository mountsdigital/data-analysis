package com.mounts.dataanalysis.controllers;

import com.chan.jerk.Controller;
import com.chan.jerk.Layout;
import com.mounts.dataanalysis.action.DownloadMemory;
import com.mounts.dataanalysis.task.DownloadTask;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

@Layout("DownloadMemoryView")
public class  DownloadMemoryController extends Controller implements Initializable {
    @FXML private TextField txfOpenFile, txfFileName;
    @FXML private Button btnOpenFile, btnDownload;
    @FXML private ToggleGroup toggleGroupType;
    @FXML private BorderPane pane;

    private boolean isAborted = false;
    private String dirPath = "";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onClose() {
        //TODO remove this process and make task cancelable
        if (pane.isDisabled()) {
            isAborted = true;
        }
    }

    @Override
    public void onLoaded(Parent root) {
        super.onLoaded(root);

        txfOpenFile.setFocusTraversable(false);
        btnOpenFile.requestFocus();
        currentStage.setResizable(false);
    }

    public void btnOpenFile_onAction() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(currentStage);

        if (selectedDirectory != null) {
            dirPath = selectedDirectory.getPath();
            txfOpenFile.setText(dirPath);
        }
    }

    public void btnOpen_onAction() {
        if (dirPath.isEmpty()) return;
        if (txfFileName.getText().isEmpty()) return;

        String selectedToggle = toggleGroupType.getSelectedToggle().getUserData().toString();

        new DownloadMemory(this, dirPath).run(txfFileName.getText(), selectedToggle);
    }

    public void btnClose_onAction() {
        currentStage.close();
    }

    public void setDownloadButtonStatusLoading(boolean statusLoading) {
        if (statusLoading) {
            pane.setDisable(true);
            btnDownload.setText("Downloading...");
        } else {
            pane.setDisable(false);
            btnDownload.setText("Download");
        }
    }

    @SuppressWarnings("unchecked")
    public void setDownloadResult(DownloadTask.DownloadTaskResult result) {
        result.setAborted(isAborted);
        this.result.onResult(result);
        currentStage.close();
    }
}
