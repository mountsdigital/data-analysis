package com.mounts.dataanalysis.task;

import com.chan.fxtask.Task;
import com.mounts.dataanalysis.backend.datapackage.DataPackage;
import com.mounts.dataanalysis.backend.datapackage.DataPackageConfig;
import com.mounts.dataanalysis.backend.DataReader;
import com.mounts.dataanalysis.backend.datapackage.DataPackageWriter;
import com.mounts.dataanalysis.controllers.DownloadMemoryController;

public class DownloadTask extends Task<String, Void, Object> {
    private DownloadMemoryController controller;
    private String path;
    private DataPackageConfig config;

    private boolean isAborted = false;

    public DownloadTask(DownloadMemoryController _controller, String _path, DataPackageConfig _config) {
        controller = _controller;
        path = _path;
        config = _config;
    }

    public void abort() {
        isAborted = true;
    }

    @Override
    public void onPreExecute() {
        controller.setDownloadButtonStatusLoading(true);
    }

    @Override
    public Object doInBackground(String... params) {
        DataReader reader = new DataReader(path, config);

        try {
            DataPackage dataPackage =  reader.read();
            if (config.shouldGenerateDTW()) {
                dataPackage.generateDTR();
            }

            DataPackageWriter writer = new DataPackageWriter(dataPackage, config);
            writer.write(params[0], params[1]);

            return dataPackage;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onPostExecute(Object... params) {
        if (isAborted) {
            // TODO aborted delete download memory
        }
        controller.setDownloadButtonStatusLoading(false);
        controller.setDownloadResult(new DownloadTaskResult(params[0] != null, (DataPackage) params[0]));
    }

    @Override
    public void progressCallback(Void... voids) {

    }

    public class DownloadTaskResult {
        private boolean isSuccess;
        private DataPackage dataPackage;
        private boolean isAborted;

        public DownloadTaskResult(boolean b, DataPackage p) {
            isSuccess = b;
            dataPackage = p;
        }

        public void setAborted(boolean a) {
            isAborted = a;
        }

        public boolean isAborted() {
            return isAborted;
        }

        public boolean isSuccess() {
            return isSuccess;
        }

        public DataPackage getDataPackage() {
            return dataPackage;
        }
    }
}
