package com.mounts.dataanalysis.task;

import com.chan.fxtask.Task;
import com.chan.jerk.Controller;
import com.chan.jerk.Jerk;
import com.mounts.dataanalysis.backend.datapackage.DataPackage;
import com.mounts.dataanalysis.controllers.MainController;
import com.mounts.dataanalysis.controllers.OpenMemoryController;
import com.mounts.dataanalysis.data.MemoryData;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class LoadFilesTask extends Task<Void, Void, Object> {
    private IOFileFilter filter = new IOFileFilter() {
        @Override
        public boolean accept(File file) {
            return file.isDirectory();
        }

        @Override
        public boolean accept(File dir, String name) {
            return dir.isDirectory();
        }
    };

    private Stage currentStage;
    private Controller.Result result;
    private MainController controller;

    public LoadFilesTask(MainController controller, Stage stage, Controller.Result result) {
        this.controller = controller;
        currentStage = stage;
        this.result = result;
    }

    @Override
    public void onPreExecute() {
        controller.setStatus("Loading Files...");
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object doInBackground(Void... voids) {
        Collection<File> files = FileUtils.listFilesAndDirs(new File(DataPackage.DATA_PATH + "/"), filter, filter);

        ArrayList<MemoryData>[] dataArr = new ArrayList[4];

        for(int i = 0; i < 4; i++) {
            dataArr[i] = new ArrayList<>();
        }

        for(File file: files) {
            try {
                MemoryData data = new MemoryData(file.getName());
                int index = -1;
                switch (data.getType()) {
                    case "STM":
                        index = 0;
                        break;
                    case "LTM":
                        index = 1;
                        break;
                    case "MCM":
                        index = 2;
                        break;
                    case "ELM":
                        index = 3;
                        break;
                }

                if (dataArr[index] == null) dataArr[index] = new ArrayList<>();
                dataArr[index].add(data);
            } catch (Exception e) {}
        }

        return dataArr;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onPostExecute(Object... objects) {
        Jerk openView = new Jerk(OpenMemoryController.class);
        openView.makeModelWindow(currentStage);
        controller.setStatus("");

        try {
            Controller controller = openView.start(result);
            controller.passExtras(objects[0]);
            controller.show();
        } catch (IOException e) {}
    }

    @Override
    public void progressCallback(Void... voids) {

    }
}
