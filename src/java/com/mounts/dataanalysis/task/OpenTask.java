package com.mounts.dataanalysis.task;

import com.chan.fxtask.Task;
import com.mounts.dataanalysis.backend.datapackage.DataPackage;
import com.mounts.dataanalysis.backend.datapackage.DataPackageReader;
import com.mounts.dataanalysis.controllers.OpenMemoryController;

public class OpenTask extends Task<Void, Void, Object> {
    private String name;
    private OpenMemoryController controller;

    public OpenTask(OpenMemoryController controller, String name) {
        this.controller = controller;
        this.name = name;
    }

    @Override
    public void onPreExecute() {
        controller.setStatusLoading(true);
    }

    @Override
    public Object doInBackground(Void... voids) {
        try {
            DataPackageReader reader = new DataPackageReader(name);
            return reader.read();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onPostExecute(Object... objects) {
        controller.setStatusLoading(false);
        controller.setOpenResult((DataPackage) objects[0]);
    }

    @Override
    public void progressCallback(Void... voids) {

    }
}
