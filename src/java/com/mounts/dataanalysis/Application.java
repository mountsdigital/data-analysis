package com.mounts.dataanalysis;

import com.chan.jerk.Jerk;
import com.mounts.dataanalysis.controllers.MainController;
import javafx.stage.Stage;

import java.io.IOException;

public class Application extends javafx.application.Application {
    @Override
    public void start(Stage stage) throws IOException {
        new Jerk(MainController.class, stage).start().show();
        Jerk.applyKillHandler(stage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
