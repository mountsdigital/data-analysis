package com.mounts.dataanalysis.test;

import com.mounts.dataanalysis.backend.datapackage.DataPackage;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class DataReaderReadTest {
    public static void main(String[] args) throws Exception {
        JSONObject json = new JSONObject();

        int[] i = new int[] {1,2,3,4,5,6,7,8,9,10};

        json.put("a", i);

        String jss = json.toJSONString();

        JSONObject object = (JSONObject) (new JSONParser()).parse(jss);

        int[] j = (int[] )object.get("a");

        System.out.println(Arrays.toString(j));
    }
}
