package com.mounts.dataanalysis.test;

import com.mounts.dataanalysis.backend.datapackage.DataPackage;
import com.mounts.dataanalysis.backend.datapackage.DataPackageConfig;
import com.mounts.dataanalysis.backend.DataReader;
import com.mounts.dataanalysis.backend.data.OperatorData;
import com.mounts.dataanalysis.backend.data.ShortTerm;


public class DataReaderWriteTest {
    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();

        DataPackageConfig config = new DataPackageConfig(true);
        config.addDataClass(ShortTerm.class);
        config.addDataClass(OperatorData.class);

        DataReader dr = new DataReader("/home/chan/Desktop/MY FILE/New", config);

        DataPackage dataPackage = dr.read();
        dataPackage.generateDTR();

        long stopTime = System.nanoTime();
        long elapsedTime = stopTime - startTime;
        System.out.println((double) elapsedTime / 1000000000.0);

        System.out.println("Saving");

        startTime = System.nanoTime();

//        dataPackage.save("STM001", "STM");

        stopTime = System.nanoTime();
        elapsedTime = stopTime - startTime;
        System.out.println((double) elapsedTime / 1000000000.0);
    }
}
